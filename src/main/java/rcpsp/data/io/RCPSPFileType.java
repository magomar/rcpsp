package rcpsp.data.io;

/**
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public enum RCPSPFileType implements FileType {

    SINGLE_MODE(".sm", "Single mode problems"),
    MULTI_MODE(".mm", "Multiple mode problems");
    public static final String JAXB_CONTEXT_PATH = "data.data.jaxb";
    public static final String JAXB_NAMESPACE = "rcpsp";
    private final String fileExtension;
    private final String description;
    private final FileTypeFilter fileTypeFilter;

    private RCPSPFileType(final String fileExtension, final String description) {
        this.fileExtension = fileExtension;
        this.description = description;
        fileTypeFilter = new FileTypeFilter(this);
    }

    public static RCPSPFileType fromFileExtension(String extension) {
        if (extension != null) {
            for (RCPSPFileType fileType : RCPSPFileType.values()) {
                if (extension.equalsIgnoreCase(fileType.fileExtension)) {
                    return fileType;
                }
            }
        }
        return null;

    }

    @Override
    public String getFileExtension() {
        return fileExtension;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public FileTypeFilter getFileTypeFilter() {
        return fileTypeFilter;
    }
}
