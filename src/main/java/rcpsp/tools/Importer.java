package rcpsp.tools;

import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;

import java.io.File;
import java.util.logging.Logger;

/**
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class Importer {

    static final DatasetWrapper sourceWrapper = WrapperFactory.obtainWrapper(WrapperFactory.psplib_sm);
    private static final Logger LOG = Logger.getLogger(Importer.class.getName());

    static public void importDatasetLibrary(File folder) {
    }
}
