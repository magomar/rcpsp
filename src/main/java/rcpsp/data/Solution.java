package rcpsp.data;

import rcpsp.data.jaxb.JobSchedule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Mario Gomez
 */
public final class Solution implements Comparable<Object>, Serializable {
    private static final long serialVersionUID = 1L;
    private final int[] jobsArray;
    private final int[] startTime;
    private final int[] finishTime;
    private final int[] mode;
    private final int makespan;
    private String algorithm;
    private boolean isComplete;
    private ResourcesArray[] remainingResources;

    /**
     * Constructor used to create a new solution
     *
     * @param jobsArray
     * @param startTime
     * @param finishTime
     * @param mode
     * @param makespan
     */
    public Solution(int[] jobsArray, int[] startTime, int[] finishTime, int[] mode, int makespan, String algorithm, ResourcesArray[] remainingResources) {
        this.jobsArray = jobsArray;
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.mode = mode;
        this.makespan = makespan;
        this.algorithm = algorithm;
        this.remainingResources = remainingResources;
        isComplete = true;
    }


    /**
     * Constructor used to create a solution container (solution is not really provided yet)
     *
     * @param numJobs
     * @param timeHorizon
     */
    public Solution(int numJobs, int timeHorizon) {
        this.jobsArray = new int[numJobs];
        this.startTime = new int[numJobs];
        this.finishTime = new int[numJobs];
        this.mode = new int[numJobs];
        this.makespan = timeHorizon + 1;
        this.remainingResources = null;
        isComplete = false;
    }

    /**
     * @return the makespan
     */
    public int getMakespan() {
        return makespan;
    }

    /**
     * @return whether this solution is complete or not
     */
    public boolean isComplete() {
        return isComplete;
    }

    /**
     * @return the algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    public int[] getJobsArray() {
        return jobsArray;
    }

    public List<JobSchedule> getJobSchedules() {
        List<JobSchedule> jobScheduleList = new ArrayList<>();
        for (int i = 0; i < jobsArray.length; i++) {
            JobSchedule js = new JobSchedule();
            js.setId(jobsArray[i]);
            js.setStartTime(startTime[i]);
            js.setFinishTime(finishTime[i]);
            js.setMode(mode[i]);
            jobScheduleList.add(js);
        }
        return jobScheduleList;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        ArrayList<Integer> jobs = new ArrayList<Integer>();
        for (int i = 1; i < jobsArray.length; i++) {
            jobs.add(jobsArray[i]);
        }
        s.append(algorithm);
        s.append(": ");
        s.append(jobs.toString());
        s.append("(");
        s.append(makespan);
        s.append(")");
        return s.toString();
    }

    public String toStringVerbose() {
        StringBuilder s = new StringBuilder();
        s.append("\nPROJECT SCHEDULE\n__________________________________________________________________________\nJob\tST\tFT\tmode\n");
        for (int i = 1; i < jobsArray.length; i++) {
            int jobID = jobsArray[i];
            s.append(jobID);
            s.append("\t");
            s.append(startTime[jobID]);
            s.append("\t");
            s.append(finishTime[jobID]);
            s.append("\t");
            s.append(mode[jobID] + "\n");
        }
        return s.toString();
    }

    /**
     * @return resource availability chart using plain text
     */
    public String toGanttDiagram() {
        StringBuilder s = new StringBuilder();
        s.append("Project Schedule [Makespan = " + makespan + "] \n");
        for (int i = 1; i < jobsArray.length; i++) {
            int j = jobsArray[i];
            s.append(j);
            s.append("\t");
            for (int t = 0; t < startTime[j]; t++) {
                s.append(" ");
            }
            for (int t = startTime[j]; t < finishTime[j]; t++) {
                s.append("*");
            }
            s.append(" ");
            s.append(startTime[j]);
            s.append(" > ");
            s.append(finishTime[j]);
            s.append(" (m");
            s.append(mode[j]);
            s.append(")\n");
        }
        return s.toString();
    }

    /**
     * @return resource availability chart using plain text
     */
    public String toResourceAvailability() {
        StringBuilder s = new StringBuilder();
        s.append("Resource Availability\n");
        ArrayList<Integer> jobsSortedByFT = new ArrayList<Integer>(); //jobs with a different finish time
        for (int j = 0; j < finishTime.length; j++) {
            jobsSortedByFT.add(j);
        }
        Collections.sort(jobsSortedByFT, new FinishTimeComparator(finishTime));
        for (Integer job : jobsSortedByFT) {
            s.append("j");
            s.append(job);
            s.append("\tFT: ");
            s.append(finishTime[job.intValue()]);
            s.append("\t");
            s.append(remainingResources[job.intValue()].toString());
            s.append("\n");
        }

        return s.toString();
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Object o) {
        Solution solution2 = (Solution) o;
        return this.getMakespan() - solution2.getMakespan();
    }


}
