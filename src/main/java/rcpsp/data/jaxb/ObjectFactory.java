package rcpsp.data.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the data.data.jaxb package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: data.data.jaxb
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DatasetLibrary }
     */
    public DatasetLibrary createDatasetLibrary() {
        return new DatasetLibrary();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.Dataset }
     */
    public Dataset createDataset() {
        return new Dataset();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.Project }
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link BenchmarkResults }
     */
    public BenchmarkResults createBenchmarkResults() {
        return new BenchmarkResults();
    }

    /**
     * Create an instance of {@link BenchmarkSolution }
     */
    public BenchmarkSolution createBenchmarkSolution() {
        return new BenchmarkSolution();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.Job }
     */
    public Job createJob() {
        return new Job();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.Resource }
     */
    public Resource createResource() {
        return new Resource();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.JobSchedule }
     */
    public JobSchedule createJobSchedule() {
        return new JobSchedule();
    }

    /**
     * Create an instance of {@link ProjectSchedule }
     */
    public ProjectSchedule createProjectSchedule() {
        return new ProjectSchedule();
    }

    /**
     * Create an instance of {@link rcpsp.data.jaxb.Mode }
     */
    public Mode createMode() {
        return new Mode();
    }

}
